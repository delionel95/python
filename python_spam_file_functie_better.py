from python_spam_regel_functie import line_is_spam

def file_is_spam(Filepath):
    is_spam = False
    with open(Filepath) as fh:
        for line in fh.readlines():
            is_spam = line_is_spam(line.strip())
    return is_spam
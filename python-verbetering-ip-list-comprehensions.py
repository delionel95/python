from aha_ip_adres_lezen import read_ip

def read_ips():
    ipLijst = []
    with open('ips.txt') as fh:
        for line in fh.readlines():
            ipLijst.append(read_ip(line))
        return ipLijst
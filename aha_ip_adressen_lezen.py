def read_ip(line):
    return [int(elem) for elem in line.split(".")]

def read_ips():
    LegeLijst = []
    with open('ips.txt') as fh:
        for line in fh.readlines():
            LegeLijst.append(read_ip(line))
        return LegeLijst
print(read_ips())
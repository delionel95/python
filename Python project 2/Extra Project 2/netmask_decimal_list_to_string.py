def netmask_decimal_list_to_string(numberlist):
    binary_netmask = ""
    for getallen in numberlist:
        binary_netmask += (f"{getallen:08b}")
    return binary_netmask
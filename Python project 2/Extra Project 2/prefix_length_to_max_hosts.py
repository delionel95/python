from netmask_decimal_list_to_string import netmask_decimal_list_to_string

def prefix_length_to_max_hosts(netmask):
    teller = 0
    binary_netmask = netmask_decimal_list_to_string(netmask)
    for character in binary_netmask:
        if character == "1":
            teller += 1
    host_bits = 32 - int(teller)
    aantal_hosts = (2 ** host_bits) - 2 
    return aantal_hosts
from netmask_decimal_list_to_string import netmask_decimal_list_to_string

def one_bits_in_netmask(numberlist):
    counter = 0
    binary_netmask = netmask_decimal_list_to_string(numberlist)
    for symbol in binary_netmask:
        if symbol == "1":
            counter += 1
    return counter
def lijst_NOT(lijst):
    byteteller = 0
    NOT_lijst = [0, 0, 0, 0]
    binary_byte = ""
    for getallen in lijst:
        binary_byte = (f"{getallen:08b}")
        NOT_byte = ""
        for bits in binary_byte:
            if bits == "0":
                NOT_byte += "1"
            if bits == "1":
                NOT_byte += "0"
        NOT_lijst[byteteller] = int(NOT_byte, 2)
        byteteller += 1
    return NOT_lijst
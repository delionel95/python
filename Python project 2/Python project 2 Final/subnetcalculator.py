from ask_for_number_sequence import ask_for_number_sequence
from is_valid_ip_address import is_valid_ip_address
from is_valid_netmask import is_valid_netmask
from netmask_decimal_list_to_string import netmask_decimal_list_to_string
#Hierboven worden externe functie's geinporteerd

#Deze functie is om 1 bits te tellen in een netwerkmask
def one_bits_in_netmask(numberlist):
    counter = 0
    binary_netmask = netmask_decimal_list_to_string(numberlist)
    for symbol in binary_netmask:
        if symbol == "1":
            counter += 1
    return counter

#Functie om lijsten te ANDen
def lijsten_AND(lijst1, lijst2):
    teller = 0
    adres = []
    for getallen in lijst1:
        adres.append(getallen & lijst2[teller])
        teller += 1
    return adres

#functie om lijst te NOTen
def lijst_NOT(lijst):
    byteteller = 0
    NOT_lijst = [0, 0, 0, 0]
    binary_byte = ""
    for getallen in lijst:
        binary_byte = (f"{getallen:08b}")
        NOT_byte = ""
        for bits in binary_byte:
            if bits == "0":
                NOT_byte += "1"
            if bits == "1":
                NOT_byte += "0"
        NOT_lijst[byteteller] = int(NOT_byte, 2)
        byteteller += 1
    return NOT_lijst

#Functie om lijst te ORen
def lijsten_OR(lijst1, lijst2):
    teller = 0
    adres = []
    for getallen in lijst1:
        adres.append(getallen | lijst2[teller])
        teller += 1
    return adres

#Functie om met de netmask de max host te berekenen
def prefix_length_to_max_hosts(netmask):
    teller = 0
    binary_netmask = netmask_decimal_list_to_string(netmask)
    for character in binary_netmask:
        if character == "1":
            teller += 1
    host_bits = 32 - int(teller)
    aantal_hosts = (2 ** host_bits) - 2 
    return aantal_hosts

#Begin zichtbare code

print("Welkom bij de subnet calculator van Lionel Der Boven!\n")

#Loop om te blijven loopen tot de gebruiker het programma wilt afsluiten
Program_active = True
while Program_active:
    is_valid = False
    #Controle loop
    while is_valid == False:
        IP_adres = ask_for_number_sequence("Wat is het IP-adres?")
        Subnetmask = ask_for_number_sequence("Wat is het subnetmask?")
        if is_valid_ip_address(IP_adres) == True and is_valid_ip_address(Subnetmask) == True and is_valid_netmask(Subnetmask) == True:
            is_valid = True
        if is_valid == False:
            print("IP-adres of subnetmask zijn niet geldig. probeer opnieuw.\n")
    print("IP-adres en subnetmasker zijn geldig.\n")

    #Uitkomsten in variabelen met int lijsten of string format
    netwerk_adres = lijsten_AND(IP_adres, Subnetmask)
    netwerk_adres_string = ".".join(map(str, netwerk_adres))
    wildcard = lijst_NOT(Subnetmask)
    wildcard_string = ".".join(map(str, wildcard))
    broadcastadres = lijsten_OR(netwerk_adres, wildcard)
    broadcastadres_string = ".".join(map(str, broadcastadres))

    #Uitkomst laten zien aan gebruiker
    print(f"De lengte van het subnetmasker is {one_bits_in_netmask(Subnetmask)}")
    print(f"Het adres van het subnet is {netwerk_adres_string}")
    print(f"Het wildcardmasker is {wildcard_string}")
    print(f"Het broadcastadres is {broadcastadres_string}")
    print(f"Het maximaal aantal hosts op dit subnet is {prefix_length_to_max_hosts(Subnetmask)}\n")
    
    #Vragen om opnieuw te starten, anders programma afsluiten.
    opnieuw = input("Wil je nog een subnet berekenen typ 'j', druk gewoon enter om door te gaan.\n")
    if opnieuw == "j" or opnieuw == "J":
        Program_active = True
        print("Het programma word opnieuw gestart.\n")
    else:
        Program_active = False
        print("\nHet programma word afgesloten. Bedankt voor dit programma te gebruiken en tot ziens!\n")
        input("Druk enter om af te sluiten...")
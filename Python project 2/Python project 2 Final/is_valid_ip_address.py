def is_valid_ip_address(numberlist):
    is_valid_ip = True
    for getallen in numberlist:
        if getallen < 0 or getallen > 255 or len(numberlist) != 4:
            is_valid_ip = False
    return is_valid_ip
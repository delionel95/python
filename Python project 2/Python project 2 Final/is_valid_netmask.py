from netmask_decimal_list_to_string import netmask_decimal_list_to_string

def is_valid_netmask(numberlist):
    binary_netmask = netmask_decimal_list_to_string(numberlist)
    checking_ones = True
    is_valid = True
    for symbol in binary_netmask:
        if symbol == "0" and checking_ones == True:
            checking_ones = False
        if checking_ones == False and symbol == "1":
            is_valid = False
        if binary_netmask == "00000000000000000000000000000000" or binary_netmask == "11111111111111111111111111111111":
            is_valid = False
    return is_valid
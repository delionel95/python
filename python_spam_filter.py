import python_spam_file_functie
import os

def spam_filter():
    path = input("Welke directory wil je analyseren?\n")
    for bestand in os.listdir(path):
        absoluutpath = os.path.join(path,bestand)
        if python_spam_file_functie.file_is_spam(bestand): #if python_spam_file_functie.file_is_spam(path) == True:
            print(f"{absoluutpath}: spam")
        else:
            print(f"{absoluutpath}: geen spam")